﻿using System;

namespace JobTime.Utils
{
    /// <summary>
    /// Помечает свойство, что оно является путём до папки или файла
    /// </summary>
    public class PathAttribute : Attribute
    {
    }
}