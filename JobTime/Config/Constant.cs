﻿namespace JobTime.Config
{
    internal class Constant
    {
        public class FolderName
        {
            public const string DataFolderName = "JobTime";
        }

        public class FileName
        {
            public const string LastTimeStampFile = "timeStamp.xml";
        }
    }
}
