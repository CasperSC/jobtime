using System.IO;

namespace JobTime.Config
{
    internal class AppFiles
    {
        public AppFiles(AppFolder folders)
        {
            string appData = folders.GetPath(JobTimeFolder.AppData);
            LastStateFile = Path.Combine(appData, Constant.FileName.LastTimeStampFile);
        }

        public string LastStateFile { get; private set; }
    }
}