﻿using System;
using System.IO;
using JobTime.Utils;

namespace JobTime.Config
{
    internal class AppFolder : AppFolderBase<JobTimeFolder>
    {
        public AppFolder()
        {
            string appData = Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                Constant.FolderName.DataFolderName);

            AddFolder(JobTimeFolder.AppData, appData);
        }
    }
}
