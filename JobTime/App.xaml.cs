﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows;
using JobTime.Config;
using JobTime.Model.Settings;
using JobTime.Utils.Serialization;
using JobTime.ViewModel;

namespace JobTime
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private async void Application_Startup(object sender, StartupEventArgs e)
        {
            await LocalConfiguration.Instance.Folders.CheckFoldersAsync();
            ViewModelLocator locator = GetLocator();

            var timePanel = locator.Main.StartWorkTimePanel;
            LastState state = LoadLastState();
            if (state != null && state.LastLaunchDate.Date == DateTime.Now.Date)
            {
                timePanel.SelectedHourIndex = state.Hour;
                timePanel.SelectedMinuteIndex = state.Minute;
            }

            locator.Main.DataLoaded = true;

            Current.Exit += Current_Exit;
        }

        private void Current_Exit(object sender, ExitEventArgs e)
        {
            try
            {
                SaveLastState();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private LastState LoadLastState()
        {
            var localConfig = LocalConfiguration.Instance;
            LastState state = null;
            if (File.Exists(localConfig.Files.LastStateFile))
            {
                state = DataSerializer.LoadObject<LastState>(localConfig.Files.LastStateFile);
            }

            return state;
        }

        public void SaveLastState()
        {
            ViewModelLocator locator = GetLocator();
            var timePanel = locator.Main.StartWorkTimePanel;

            var state = new LastState();
            state.Hour = timePanel.CurrentHour;
            state.Minute = timePanel.CurrentMinute;
            state.LastLaunchDate = DateTime.Now;

            var localConfig = LocalConfiguration.Instance;
            DataSerializer.SaveObject(state, localConfig.Files.LastStateFile);
        }

        private ViewModelLocator GetLocator()
        {
            return (ViewModelLocator)Resources["Locator"];
        }
    }
}
