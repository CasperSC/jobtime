﻿using GalaSoft.MvvmLight.Messaging;

namespace JobTime.Mvvm.Messaging
{
    public class NotificationMessageEx<TData> : NotificationMessage
    {
        /// <summary>
        /// Initializes a new instance of the NotificationMessage class.
        /// </summary>
        /// <param name="notification">A string containing any arbitrary message to be
        ///             passed to recipient(s)</param>
        public NotificationMessageEx(string notification) : base(notification)
        {
        }

        /// <summary>
        /// Initializes a new instance of the NotificationMessage class.
        /// </summary>
        /// <param name="sender">The message's sender.</param><param name="notification">A string containing any arbitrary message to be
        ///             passed to recipient(s)</param>
        public NotificationMessageEx(object sender, string notification) : base(sender, notification)
        {
        }

        /// <summary>
        /// Initializes a new instance of the NotificationMessage class.
        /// </summary>
        /// <param name="sender">The message's sender.</param><param name="target">The message's intended target. This parameter can be used
        ///             to give an indication as to whom the message was intended for. Of course
        ///             this is only an indication, amd may be null.</param><param name="notification">A string containing any arbitrary message to be
        ///             passed to recipient(s)</param>
        public NotificationMessageEx(object sender, object target, string notification) : base(sender, target, notification)
        {
        }

        /// <summary>
        /// Initializes a new instance of the NotificationMessage class.
        /// </summary>
        /// <param name="notification">A string containing any arbitrary message to be
        ///             passed to recipient(s)</param>
        /// <param name="data"></param>
        public NotificationMessageEx(string notification, TData data) : this(null, null, notification, data)
        {
        }

        /// <summary>
        /// Initializes a new instance of the NotificationMessage class.
        /// </summary>
        /// <param name="sender">The message's sender.</param><param name="notification">A string containing any arbitrary message to be
        ///             passed to recipient(s)</param>
        /// <param name="data"></param>
        public NotificationMessageEx(object sender, string notification, TData data) : this(sender, null, notification, data)
        {
        }

        public NotificationMessageEx(TData data) : this(null, null, null, data)
        {
        }

        /// <summary>
        /// Initializes a new instance of the NotificationMessage class.
        /// </summary>
        /// <param name="sender">The message's sender.</param><param name="target">The message's intended target. This parameter can be used
        ///             to give an indication as to whom the message was intended for. Of course
        ///             this is only an indication, amd may be null.</param><param name="notification">A string containing any arbitrary message to be
        ///             passed to recipient(s)</param>
        /// <param name="data"></param>
        public NotificationMessageEx(object sender, object target, string notification, TData data) : base(sender, target, notification)
        {
            Data = data;
        }

        public TData Data { get; protected set; }
    }
}
