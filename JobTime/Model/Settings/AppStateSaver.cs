using System.Threading.Tasks;
using System.Windows;
using JobTime.Model.Settings.Interfaces;

namespace JobTime.Model.Settings
{
    public class AppStateSaver : IConfigSaver
    {
        private readonly object _saveSync = new object();

        public async Task Save()
        {
            await Task.Run(() =>
            {
                var app = (App)Application.Current;
                lock (_saveSync)
                {
                    app.SaveLastState();
                }
            });
        }
    }
}