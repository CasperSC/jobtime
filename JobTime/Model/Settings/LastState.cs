﻿using System;

namespace JobTime.Model.Settings
{

    public class LastState
    {
        public DateTime LastLaunchDate { get; set; }

        public int Hour { get; set; }

        public int Minute { get; set; }
    }
}
