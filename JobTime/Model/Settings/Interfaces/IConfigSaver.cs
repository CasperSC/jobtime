using System.Threading.Tasks;

namespace JobTime.Model.Settings.Interfaces
{
    public interface IConfigSaver
    {
        Task Save();
    }
}