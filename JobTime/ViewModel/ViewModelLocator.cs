using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using JobTime.Controls.TimeView.ViewModel;
using JobTime.Model.Settings;
using JobTime.Model.Settings.Interfaces;
using JobTime.ViewModel.Formatters.Implementation;
using JobTime.ViewModel.Formatters.Interfaces;
using Microsoft.Practices.ServiceLocation;

namespace JobTime.ViewModel
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator
    {
        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);
            
            SimpleIoc.Default.Register<ITextFormatter<TimeViewPanelViewModel>, RemainTimeTextFormatter>(true);
            SimpleIoc.Default.Register<IConfigSaver, AppStateSaver>(true);
            SimpleIoc.Default.Register<MainViewModel>(true);
        }

        public MainViewModel Main
        {
            get { return ServiceLocator.Current.GetInstance<MainViewModel>(); }
        }
    }
}