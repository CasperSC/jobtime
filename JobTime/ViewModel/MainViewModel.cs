using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using JobTime.Controls.TimeSettings.ViewModel;
using JobTime.Controls.TimeView.ViewModel;
using JobTime.Model.Settings.Interfaces;
using JobTime.Mvvm.Messaging;
using JobTime.ViewModel.Formatters.Implementation;
using JobTime.ViewModel.Formatters.Interfaces;
using JobTime.ViewModel.Interfaces;

namespace JobTime.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase, IViewState
    {
        private readonly IConfigSaver _appStateSaver;
        private readonly ITextFormatter<TimeViewPanelViewModel> _timeRemainFormatter;
        private bool _dataLoaded;

        /// <summary> ���-�� ������� ����� � ������� ���. </summary>
        private const int WorkingMinutes = 9 * 60;
        /// <summary> ���-�� �����, ������� ������ ����. </summary>
        private const int LunchDurationMinutes = 60;
        /// <summary> ���-�� �����, ���������� �� ������ �� �����. </summary>
        private const int BeforeLunchMinutes = 4 * 60;

        private readonly DispatcherTimer _timer;
        private string _title;
        private readonly NotificationViewModel _notificationViewModel;


        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel(IConfigSaver appStateSaver, ITextFormatter<TimeViewPanelViewModel> timeRemainFormatter)
        {
            _title = "Job Time";

            _appStateSaver = appStateSaver;
            _timeRemainFormatter = timeRemainFormatter;
            _notificationViewModel = new NotificationViewModel(timeRemainFormatter);

            StartWorkTimePanel = new TimeSettingsPanelViewModel("����� ������� �� ������:");
            StartWorkTimePanel.TimeUpdated += TimeSettingsPanel_TimeUpdated;
            StartLunchTimePanel = new TimeSettingsPanelViewModel("����� ������ �����:");

            EndWorkPanel = new TimeViewPanelViewModel("����� ���������� �������� ���");
            StartLunchPanel = new TimeViewPanelViewModel("����� ������ �����");
            EndLunchPanel = new TimeViewPanelViewModel("����� ��������� �����");
            TimeHasPassedPanel = new TimeViewPanelViewModel("����� ������ � ������ �������� ���");
            TimeLeftPanel = new TimeViewPanelViewModel("�������� �������");

            UpdateTime(StartWorkTimePanel.CurrentHour, StartWorkTimePanel.CurrentMinute);

            _timer = new DispatcherTimer();
            _timer.Interval = TimeSpan.FromSeconds(1d);
            _timer.Tick += Timer_Tick;
            _timer.Tag = (int?)StartWorkTimePanel.CurrentMinute;
            _timer.Start();
        }

        ~MainViewModel()
        {
            _timer.Stop();
        }
        
        public string Title
        {
            get { return _title; }
            set
            {
                _title = value;
                if (_title != value)
                {
                    RaisePropertyChanged();
                }
            }
        }

        public bool DataLoaded
        {
            get { return _dataLoaded; }
            set
            {
                if (_dataLoaded != value)
                {
                    _dataLoaded = value;
                    RaisePropertyChanged();
                }
            }
        }

        public bool ViewLoaded { get; set; }

        public bool IsUserNotified { get; set; }

        /// <summary>������������ ���������� �������.</summary>
        public TimeSettingsPanelViewModel StartWorkTimePanel { get; private set; }

        /// <summary>����� ������ �����.</summary>
        public TimeSettingsPanelViewModel StartLunchTimePanel { get; private set; }

        /// <summary>����� ���������� �������� ���.</summary>
        public TimeViewPanelViewModel EndWorkPanel { get; private set; }

        /// <summary>����� ������ �����.</summary>
        public TimeViewPanelViewModel StartLunchPanel { get; private set; }

        /// <summary>����� ��������� �����.</summary>
        public TimeViewPanelViewModel EndLunchPanel { get; private set; }

        /// <summary>����� ������ � ������ �������� ���.</summary>
        public TimeViewPanelViewModel TimeHasPassedPanel { get; private set; }

        /// <summary>����� ������ � ������ �������� ���.</summary>
        public TimeViewPanelViewModel TimeLeftPanel { get; private set; }

        private void Timer_Tick(object sender, EventArgs e)
        {
            Debug.WriteLine("Timer_Tick");
            var timer = (DispatcherTimer)sender;
            int? currentMinute = DateTime.Now.Minute;

            int? lastMinute = (int?)timer.Tag;

            if (lastMinute.Value != currentMinute.Value)
            {
                timer.Tag = currentMinute;
                UpdateTime(StartWorkTimePanel.CurrentHour, StartWorkTimePanel.CurrentMinute);

                var minuteSpan = TimeSpan.FromMinutes(1d);
                if (timer.Interval != minuteSpan)
                {
                    timer.Interval = minuteSpan;
                }
            }
        }

        private async void TimeSettingsPanel_TimeUpdated(object sender, EventArgs e)
        {
            IsUserNotified = false;

            var time = (TimeSettingsPanelViewModel)sender;
            UpdateTime(time.CurrentHour, time.CurrentMinute);
            await _appStateSaver.Save();
        }

        private void UpdateTime(int startHour, int startMinute)
        {
            Debug.WriteLine("UpdateTime: startHour = {0}, startMinute = {1}", startHour, startMinute);

            DateTime now = DateTime.Now;
            DateTime startTime = new DateTime(now.Year, now.Month, now.Day, startHour, startMinute, 0);
            DateTime endWork = startTime.AddMinutes(WorkingMinutes);
            TimeSpan timeHasPassed = now - startTime;

            DateTime startLunchWork = startTime.AddMinutes(BeforeLunchMinutes);
            DateTime endLunchWork = startTime.AddMinutes(BeforeLunchMinutes + LunchDurationMinutes);

            EndWorkPanel.Time = endWork.ToShortTimeString();
            StartLunchPanel.Time = startLunchWork.ToShortTimeString();
            EndLunchPanel.Time = endLunchWork.ToShortTimeString();

            string passedTimeText = FormatTime(timeHasPassed);
            TimeHasPassedPanel.Time = timeHasPassed.Ticks > 0L ? passedTimeText : "-" + passedTimeText;

            TimeSpan workPeriod = TimeSpan.FromMinutes(WorkingMinutes + 1);
            TimeSpan timeRemain = workPeriod - timeHasPassed;
            string timeRemainText = FormatTime(timeRemain);
            TimeLeftPanel.Time = timeRemain.Ticks > 0L ? timeRemainText : "-" + timeRemainText;

            if (timeRemain.Ticks <= 0 && DataLoaded && ViewLoaded)
            {
                if (!IsUserNotified)
                {
                    IsUserNotified = true;
                    
                    Messenger.Default.Send(
                        new NotificationMessageEx<NotificationViewModel>(_notificationViewModel));
                }

                _notificationViewModel.Update(TimeLeftPanel);
            }
        }

        private string FormatTime(TimeSpan time)
        {
            return time.ToString(@"hh\:mm");
        }
    }
}