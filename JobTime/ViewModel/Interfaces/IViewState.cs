namespace JobTime.ViewModel.Interfaces
{
    public interface IViewState
    {
        bool ViewLoaded { get; set; }
    }
}