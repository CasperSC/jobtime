﻿using GalaSoft.MvvmLight;
using JobTime.Controls.TimeView.ViewModel;
using JobTime.ViewModel.Formatters.Interfaces;

namespace JobTime.ViewModel
{
    public class NotificationViewModel : ViewModelBase
    {
        private readonly ITextFormatter<TimeViewPanelViewModel> _formatter;
        private string _text;

        public NotificationViewModel(ITextFormatter<TimeViewPanelViewModel> formatter)
        {
            _formatter = formatter;
        }

        public void Update(TimeViewPanelViewModel data)
        {
            Text = _formatter.Format(data);
        }

        public string Text
        {
            get { return _text; }
            private set
            {
                _text = value;
                RaisePropertyChanged();
            }
        }
    }
}
