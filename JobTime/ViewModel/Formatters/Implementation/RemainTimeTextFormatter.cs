﻿using JobTime.Controls.TimeView.ViewModel;
using JobTime.ViewModel.Formatters.Interfaces;

namespace JobTime.ViewModel.Formatters.Implementation
{
    public class RemainTimeTextFormatter : ITextFormatter<TimeViewPanelViewModel>
    {
        public string Format(TimeViewPanelViewModel data)
        {
            return string.Format("Время вышло. Текущее значение панели \"{0}\": {1}", data.Title, data.Time);
        }
    }
}
