﻿namespace JobTime.ViewModel.Formatters.Interfaces
{
    public interface ITextFormatter<in TData>
    {
        string Format(TData data);
    }
}
