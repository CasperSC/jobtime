﻿using System.Windows;
using GalaSoft.MvvmLight.Messaging;
using JobTime.Mvvm.Messaging;
using JobTime.ViewModel;
using JobTime.ViewModel.Interfaces;

namespace JobTime.Views
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Loaded += MainWindow_Loaded;

            Messenger.Default.Register<NotificationMessageEx<NotificationViewModel>>(this, NotificationMessageReceived);
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            var viewState = (IViewState)DataContext;
            viewState.ViewLoaded = true;
        }

        private void NotificationMessageReceived(NotificationMessageEx<NotificationViewModel> message)
        {
            var messageWindow = new NotificationWindow();
            messageWindow.Owner = this;
            messageWindow.DataContext = message.Data;
            messageWindow.Show();
        }
    }
}
