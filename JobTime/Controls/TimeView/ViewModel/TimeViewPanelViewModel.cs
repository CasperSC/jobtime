﻿using System;
using GalaSoft.MvvmLight;

namespace JobTime.Controls.TimeView.ViewModel
{
    public class TimeViewPanelViewModel : ViewModelBase
    {
        private string _time;
        private string _title;

        public TimeViewPanelViewModel(string title, string time)
        {
            Title = title;
            Time = time;
        }

        public TimeViewPanelViewModel(string title)
            : this(title, GetTime(DateTime.MinValue))
        {
        }

        public string Title
        {
            get { return _title; }
            set
            {
                _title = value;
                RaisePropertyChanged();
            }
        }

        public string Time
        {
            get { return _time; }
            set
            {
                _time = value;
                RaisePropertyChanged();
            }
        }

        private static string GetTime(DateTime time)
        {
            return time.ToShortTimeString();
        }
    }
}
