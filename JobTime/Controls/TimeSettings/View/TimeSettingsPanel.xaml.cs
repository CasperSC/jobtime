﻿using System.Windows.Controls;

namespace JobTime.Controls.TimeSettings.View
{
    /// <summary>
    /// Interaction logic for TimeSettingsPanel.xaml
    /// </summary>
    public partial class TimeSettingsPanel : UserControl
    {
        public TimeSettingsPanel()
        {
            InitializeComponent();
        }
    }
}
