﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace JobTime.Controls.TimeSettings.ViewModel
{
    public class TimeSettingsPanelViewModel : ViewModelBase
    {
        private int _selectedHourIndex;
        private int _selectedMinuteIndex;
        private string _title;

        public TimeSettingsPanelViewModel(string title, DateTime dateTime)
        {
            _title = title;
            Hours = new ObservableCollection<int>(Enumerable.Range(0, 24));
            Minutes = new ObservableCollection<int>(Enumerable.Range(0, 60));

            _selectedHourIndex = Hours.IndexOf(dateTime.Hour);
            _selectedMinuteIndex = Minutes.IndexOf(dateTime.Minute);

            ResetMinutesCommand = new RelayCommand(ResetMinutes);
        }

        public TimeSettingsPanelViewModel(string title)
            : this(title, DateTime.Now)
        {
        }

        public event EventHandler TimeUpdated;

        public ICommand ResetMinutesCommand { get; private set; }

        public string Title
        {
            get { return _title; }
            set
            {
                _title = value;
                RaisePropertyChanged();
            }
        }

        public ObservableCollection<int> Hours { get; private set; }

        public ObservableCollection<int> Minutes { get; private set; }

        public int SelectedHourIndex
        {
            get { return _selectedHourIndex; }
            set
            {
                _selectedHourIndex = value;
                RaisePropertyChanged();
                OnTimeUpdated();
            }
        }

        public int SelectedMinuteIndex
        {
            get { return _selectedMinuteIndex; }
            set
            {
                _selectedMinuteIndex = value;
                RaisePropertyChanged();
                OnTimeUpdated();
            }
        }

        public int CurrentHour
        {
            get { return Hours[_selectedHourIndex]; }
        }

        public int CurrentMinute
        {
            get { return Minutes[_selectedMinuteIndex]; }
        }

        protected void OnTimeUpdated()
        {
            var handler = TimeUpdated;
            if (handler != null)
                handler(this, EventArgs.Empty);
        }

        private void ResetMinutes()
        {
            SelectedMinuteIndex = 0;
        }
    }
}
